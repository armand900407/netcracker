# NetCracker Java Exercises.

By Engr. Armando Perea Sanchez.

[![N|Solid](https://media.glassdoor.com/sql/16833/netcracker-technology-squarelogo-1457104886435.png)](https://nodesource.com/products/nsolid)[![Build Status](https://rgprincipal.com/es/wp-content/uploads/2018/12/Logo-de-Java-portada-250x122.jpg)](https://travis-ci.org/joemccann/dillinger)

These are the class excersices defined by NetCracker.

Object model: 
You have two classes - Agreement and Product.
Agreement class has fields :
	name - string that has the agreement name that it generates by rule "Agreement " + current date 	in format day/month/year.
	signed by - string with name of person who signed an agreement.
	products - collection of products that are included into this agreement. 
	This collection should be having only products that are directly under agreement.
	Products that have parent product shouldn't be in this collection. 

Product class has fields :
	parent object - reference to agreement or parent product.
	products - collection of children products.
	name - string with the product name.
	price - number with product's price. Can be non integer. 

Task - create an API that 
  1) receives Agreement object and stores it into a file with agreement's name.  
  2) receives file path to agreement saved in previous point and creates Agreement object with all nested products.  
  
All files should be readable in text editor so human could understand information inside.  

Please add one generated test file to the results.  

# Development Comments

  - I created a new Maven Project Using SpringBoot
  - I always try to apply the SOLID pronciples the closest I can.
  - There were some doubths about some requieremnts I had to resolve with the
    tools and Knowledge I've got so far about a bussiness flow.

Please let me know if my development was accordingly and If there was a way to do it better, I would Appreciate it, no matter if I don't full fill your expectations, I'm always learning new things. Thanks for the opportunity!