package com.project.fileTransfer.model;

import java.util.List;

public class ParentProduct {
	
	public Agreement agreement;
	
	public List<Product> productList;
	
	public Double agreementProductPrice;
	
	public ParentProduct() {

	}
	
	public ParentProduct(Agreement agreement, List<Product> productList, Double agreementProductPrice) {
		super();
		this.agreement = agreement;
		this.productList = productList;
		this.agreementProductPrice = agreementProductPrice;
	}

	public Agreement getAgreement() {
		return agreement;
	}

	public void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public Double getAgreementProductPrice() {
		return agreementProductPrice;
	}

	public void setAgreementProductPrice(Double agreementProductPrice) {
		this.agreementProductPrice = agreementProductPrice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agreement == null) ? 0 : agreement.hashCode());
		result = prime * result + ((agreementProductPrice == null) ? 0 : agreementProductPrice.hashCode());
		result = prime * result + ((productList == null) ? 0 : productList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParentProduct other = (ParentProduct) obj;
		if (agreement == null) {
			if (other.agreement != null)
				return false;
		} else if (!agreement.equals(other.agreement))
			return false;
		if (agreementProductPrice == null) {
			if (other.agreementProductPrice != null)
				return false;
		} else if (!agreementProductPrice.equals(other.agreementProductPrice))
			return false;
		if (productList == null) {
			if (other.productList != null)
				return false;
		} else if (!productList.equals(other.productList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParentProduct [agreement=" + agreement + ", productList=" + productList + ", agreementProductPrice="
				+ agreementProductPrice + "]";
	}

	
}
