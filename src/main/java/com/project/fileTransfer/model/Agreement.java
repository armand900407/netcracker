package com.project.fileTransfer.model;

import java.util.List;

public class Agreement {
	
	public String name;
	
	public String signedBy;
	
	public List<Product> productCollection;
	
	public Agreement() {
		
	}

	public Agreement(String name, String signedBy, List<Product> productCollection) {
		super();
		this.name = name;
		this.signedBy = signedBy;
		this.productCollection = productCollection;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSignedBy() {
		return signedBy;
	}

	public void setSignedBy(String signedBy) {
		this.signedBy = signedBy;
	}

	public List<Product> getProductCollection() {
		return productCollection;
	}

	public void setProductCollection(List<Product> productCollection) {
		this.productCollection = productCollection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((productCollection == null) ? 0 : productCollection.hashCode());
		result = prime * result + ((signedBy == null) ? 0 : signedBy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agreement other = (Agreement) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (productCollection == null) {
			if (other.productCollection != null)
				return false;
		} else if (!productCollection.equals(other.productCollection))
			return false;
		if (signedBy == null) {
			if (other.signedBy != null)
				return false;
		} else if (!signedBy.equals(other.signedBy))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Agreement [name=" + name + ", signedBy=" + signedBy + ", productCollection=" + productCollection + "]";
	}
	
	

}
