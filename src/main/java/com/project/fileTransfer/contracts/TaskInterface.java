package com.project.fileTransfer.contracts;

import java.io.IOException;

import com.project.fileTransfer.model.Agreement;
import com.project.fileTransfer.model.ParentProduct;

public interface TaskInterface {
	
	public void showAgreementContent(String path) throws IOException;
	
	public String generateFile(Agreement agreement,ParentProduct productDetails) throws IOException;
	
	public String generateDateFormat();

}
