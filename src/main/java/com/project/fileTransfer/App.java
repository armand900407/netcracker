package com.project.fileTransfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * Hello NewYorkCoffee!
 *
 */
@SpringBootApplication
public class App {

	@Autowired
    private Environment env;
	
	public static void main( String[] args )
    {
        //SpringApplication.run(App.class, args);	
		SpringApplicationBuilder builder = new SpringApplicationBuilder(App.class);
		builder.headless(false);
		ConfigurableApplicationContext context = builder.run(args);
    }
	
	

}
