package com.project.fileTransfer.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.fileTransfer.api.Task;


@RestController
@RequestMapping("fileTransfer/v1/file")
public class FileTransferController {

	//@Autowired
	//private AdministrationDao admDao;

	@GetMapping("/generatefile")
	public List<String> generateAgreementFile() throws IOException{
		Task task = new Task();
		String path;
		path = task.createAgreementObject();
		JOptionPane.showMessageDialog(null, "JSON file generated at: "+path);
		System.out.println("JSON file generated at: "+path);
		return null;
	}
	
	@GetMapping("/getfile")
	public List<String> getAgreementFile() throws IOException{
		Task task = new Task();
		task.showAgreementContent("/home/armando/Documents/Trabajo/Agreement-25-03-2020.json");
		JOptionPane.showMessageDialog(null, "JSON file read successfully ");
		return null;
	}
	
	
}
