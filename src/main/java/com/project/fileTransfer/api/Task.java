package com.project.fileTransfer.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.fileTransfer.contracts.TaskInterface;
import com.project.fileTransfer.model.Agreement;
import com.project.fileTransfer.model.ParentProduct;
import com.project.fileTransfer.model.Product;

public class Task implements TaskInterface {

	public String generateFile(Agreement agreement,ParentProduct productDetails) throws IOException {
		// Creating Object of ObjectMapper define in Jakson Api 
        ObjectMapper Obj = new ObjectMapper(); 
        String jsonPath ="/home/armando/Documents/Trabajo/"+agreement.getName()+".json";
        try { 
            // get Oraganisation object as a json string 
            String jsonAgreement = Obj.writeValueAsString(agreement);   
            // Displaying JSON String 
            System.out.println(jsonAgreement);  
		
		FileOutputStream fos = new FileOutputStream("/home/armando/Documents/Trabajo/"+agreement.getName()+".txt");
		fos.write(jsonAgreement.getBytes());
		fos.flush();
		fos.close();
		Obj.writeValue(new File(jsonPath), agreement);//Plain JSON
        } catch (IOException e) { 
            e.printStackTrace(); 
        } 
        return jsonPath;
	}
	
	public void showAgreementContent(String path) throws IOException {
		ObjectMapper mapper = new ObjectMapper(); 
        /**
         * Read object from file
         */
		Agreement agreement = null;
        try {
        	agreement = mapper.readValue(new File(path), Agreement.class);
        } catch (Exception e) {
            e.printStackTrace();
        }   
        System.out.println("READING JSON INFORMATION...........");
        System.out.println(agreement);
         
    }
	
	public String createAgreementObject() throws IOException {
		Agreement agreement = new Agreement();
		String name = "Agreement-";
		String currentDate = generateDateFormat();
		String path;
		List<Product> productList = new ArrayList<Product>();
		productList = createProductList();
		
		agreement.setName(name+currentDate);
		agreement.setProductCollection(productList);
		agreement.setSignedBy("Armando");
		
		ParentProduct parentProduct = createParentProduct(agreement,productList);
		path = generateFile(agreement,parentProduct);
		return path;
		
	}
	
	
	public List<Product> createProductList() {
		List<Product> productList = new ArrayList<Product>();
		
		Product product1 = new Product();
		product1.setName("Buckanans");
		product1.setPrice(650.00);
		
		Product product2 = new Product();
		product2.setName("Bacardi");
		product2.setPrice(150.00);
		
		Product product3 = new Product();
		product3.setName("Appleton");
		product3.setPrice(350.00);
		
		Product product4 = new Product();
		product4.setName("Bayleys");
		product4.setPrice(450.00);
		
		Product product5 = new Product();
		product5.setName("Tonayans");
		product5.setPrice(50.00);
		
		productList.add(product1);
		productList.add(product2);
		productList.add(product3);
		productList.add(product4);
		productList.add(product5);
		
		return productList;
	}
	
	public ParentProduct createParentProduct(Agreement agreement,List<Product> productList) {
		ParentProduct productAgreement = new ParentProduct();
		productAgreement.setAgreement(agreement);
		productAgreement.setProductList(productList);
		productAgreement.setAgreementProductPrice(25000.00);
		return productAgreement;
	}
	
	public String generateDateFormat(){
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		return date;
	}
	

}
